import ReactRouterEnzymeContext from 'react-router-enzyme-context';

export const { history: historyMock } = new ReactRouterEnzymeContext();

[ 'push', 'replace' ].forEach(action => {
  historyMock[action] = jest.fn(historyMock[action]);
});

export const matchMock = {
  params: { id: 1 },
  isExact: true,
  path: '',
  url: ''
};
