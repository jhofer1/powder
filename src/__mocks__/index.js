export ConsoleMock from './console';
export FetchMock, { requestFailure, withAuthRequestHeader } from './fetch';
export LocalStorageMock from './localStorage';
export { historyMock, matchMock } from './router';
