export default class ConsoleMock {
  constructor() {
    this.windowConsole = window.console;
    Object.getOwnPropertyNames(window.console).forEach(prop => {
      _.isFunction(window.console[prop]) && (this[prop] = jest.fn());
    });
    window.console = this;
    window.console.log = this.windowConsole.log;
  }

  deactivate() {
    window.console = this.windowConsole;
  }
}
