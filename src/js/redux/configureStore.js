import { applyMiddleware, compose, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './reducers';
import sagas from './sagas';

const isProd = process.env.NODE_ENV === 'production';

export default history => {
  const sagaMiddleware = createSagaMiddleware();

  const composer = isProd ? compose : composeWithDevTools;

  const store = createStore(rootReducer, composer(applyMiddleware(sagaMiddleware)));

  let sagaTask;

  const runSagas = () => {
    sagaTask = sagaMiddleware.run(sagas, { history });
    sagaTask.done.then(runSagas);
  };

  runSagas();

  /* istanbul ignore if */
  if (module.hot) {
    module.hot.accept('./reducers', () => {
      store.replaceReducer(rootReducer);
    });
    module.hot.accept('./sagas', () => {
      sagaTask.cancel();
    });
  }

  return store;
};
