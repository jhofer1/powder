import { drain } from 'gnar-edge';

import { languages } from 'i18n';
import actions from 'actions';
import configureStore from 'js/redux/configureStore';

const { languageActions } = actions;

describe('redux language', () => {
  it('properly updates the store on setLanguage', drain(function* () {
    const store = configureStore();
    const CHINESE = languages.find(({ text }) => text === '中文').value;
    expect(CHINESE).toBe('CHINESE');
    yield store.dispatch(languageActions.setLanguage(CHINESE));
    expect(store.getState().language.get('current')).toBe(CHINESE);
  }));
});
