import { Map } from 'immutable';
import { StaticRouter } from 'react-router';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

import { ENGLISH, english } from 'i18n';
import { GET_ACCOUNT_DETAILS, SET_ACCOUNT_DETAILS, SUBMIT_LOGOUT } from 'actions';
import { accountDetails, userId, data, email } from 'mocks/constants';
import AccountDetailsView from 'views/accountDetails';

const {
  navButtons: { completeRegistrationButtonText, backButtonText, logoutButtonText, nextButtonText },
  accountDetails: { step2Content, steps }
} = english;

const mockStore = configureStore();
const muiTheme = createMuiTheme({
  typography: {
    useNextVariants: true
  }
});

class AccountDetailsViewWrapper extends AccountDetailsView {
  componentDidMount() {
    const { store } = this.props;
    this.unsubscribe = store.subscribe(() => {
      this.forceUpdate();
    });
  }

  render() {
    const { store } = this.props;
    return <AccountDetailsView email={email} store={store} />;
  }
}

const baseState = { activate: Map(), language: Map({ current: ENGLISH }) };

const getRootComponent = initialState => {
  const effectiveState = _.isFunction(initialState)
    ? initialState
    : { ...baseState, accountDetails: Map(accountDetails), ...initialState };
  const store = mockStore(effectiveState);
  const root = mount((
    <StaticRouter context={{}}>
      <MuiThemeProvider theme={muiTheme}>
        <AccountDetailsViewWrapper email={email} store={store} />
      </MuiThemeProvider>
    </StaticRouter>
  ));
  return { root, store };
};

describe('<AccountDetailsView />', () => {
  it('renders properly', () => {
    const { root } = getRootComponent();
    const stepper = root.find('Stepper');
    const buttons = root.find('Button');
    expect(stepper).toHaveLength(1);
    expect(stepper.find('StepLabel').map(node => node.children().children().last().text())).toEqual(steps);
    expect(root.find('AccountDetailsStep1')).toHaveLength(1);
    expect(buttons).toHaveLength(3);
    expect(buttons.first().text()).toBe(backButtonText);
    expect(buttons.at(1).text()).toBe(nextButtonText);
    expect(buttons.last().text()).toBe(logoutButtonText);
    expect(root.find('.overlay')).toHaveLength(0);
  });

  it("dispatches the correct action to fetch the user's details when the userId is missing from props", () => {
    const { root, store } = getRootComponent({ accountDetails: Map() });
    expect(root.find('.overlay')).toHaveLength(1);
    expect(store.getActions()).toEqual([ { type: GET_ACCOUNT_DETAILS, payload: { email } } ]);
  });

  it("updates the state after loading the user's details", () => {
    let initialState = true;
    const storeState = () => ({ ...baseState, accountDetails: initialState ? Map() : Map(accountDetails) });
    const accountFields = [ 'firstName', 'lastName', 'address1', 'address2', 'city', 'state', 'postalCode' ];
    const step1userDetailsProps = props => _.pick(props, accountFields);
    const { root, store } = getRootComponent(storeState);
    expect(step1userDetailsProps(root.find('AccountDetailsStep1').props())).toEqual({
      address1: '',
      address2: '',
      city: '',
      firstName: '',
      lastName: '',
      postalCode: '',
      state: ''
    });
    initialState = false;
    expect(root.find('.overlay')).toHaveLength(1);
    store.dispatch({ type: GET_ACCOUNT_DETAILS });
    expect(step1userDetailsProps(root.update().find('AccountDetailsStep1').props()))
      .toEqual(step1userDetailsProps(accountDetails));
    expect(root.find('.overlay')).toHaveLength(0);
  });

  it(
    'will not update account details when new props are received and the account details have already been loaded',
    () => {
      let initialState = true;
      const storeState = () =>
        ({ ...baseState, activate: Map(initialState ? {} : { userId }), accountDetails: Map(accountDetails) });
      const { root, store } = getRootComponent(storeState);
      const getStateAccountDetailsMock = jest.fn();
      const accountDetailsViewConstructor =
        Object.getPrototypeOf(root.find('AccountDetailsView').instance()).constructor;
      const { getStateAccountDetails } = accountDetailsViewConstructor;
      expect(getStateAccountDetails).toBeDefined();
      accountDetailsViewConstructor.getStateAccountDetails = getStateAccountDetailsMock;
      initialState = false;
      store.dispatch({ type: GET_ACCOUNT_DETAILS });
      expect(getStateAccountDetailsMock).not.toHaveBeenCalled();
      accountDetailsViewConstructor.getStateAccountDetails = getStateAccountDetails;
    }
  );

  it('navigates to the Account Create page when the Back button is clicked', () => {
    const { root } = getRootComponent();
    root.find('Button').first().simulate('click');
    expect(root.update().find('AccountDetailsStep0')).toHaveLength(1);
  });

  it('navigates to the placeholder page and dispatches the correct action when the Next button is clicked', () => {
    const { root, store } = getRootComponent();
    root.find('Button').at(1).simulate('click');
    expect(root.update().find('Grid').find({ alignItems: 'center' }).at(1).text()).toBe(step2Content);
    expect(store.getActions()).toEqual([ { type: SET_ACCOUNT_DETAILS, payload: { userId, data } } ]);
  });

  it(
    'navigates to the Account Linking page and dispatches the correct action when the Next button is clicked again',
    () => {
      const { root, store } = getRootComponent();
      root.find('Button').at(1).simulate('click');
      store.clearActions();
      const updatedNextButton = root.update().find('Button').at(1);
      expect(updatedNextButton.text()).toBe(completeRegistrationButtonText);
      expect(updatedNextButton.prop('disabled')).toBe(false);
      updatedNextButton.simulate('click');
      expect(root.find('AccountDetailsStep3')).toHaveLength(1);
      expect(store.getActions()).toEqual([ { type: SET_ACCOUNT_DETAILS, payload: { userId, data } } ]);
    }
  );

  it('does not dispatch an action when navigating from step 0 to step 1', () => {
    const { root, store } = getRootComponent();
    root.find('Button').first().simulate('click');
    root.update().find('Button').at(1).simulate('click');
    expect(store.getActions()).toEqual([]);
  });

  it('dispatches the correct action when the Logout button is clicked', () => {
    const { root, store } = getRootComponent();
    root.find('Button').last().simulate('click');
    expect(store.getActions()).toEqual([ { type: SUBMIT_LOGOUT, payload: {} } ]);
  });
});
