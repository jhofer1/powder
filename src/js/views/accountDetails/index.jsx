import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Stepper from '@material-ui/core/Stepper';
import handleChange from 'gnar-edge/es/handleChange';
import withStyles from '@material-ui/core/styles/withStyles';

import { Overlay } from 'components';
import actions from 'actions';
import AccountDetailsStep0 from './step0';
import AccountDetailsStep1 from './step1';
import AccountDetailsStep2 from './step2';
import AccountDetailsStep3 from './step3';
import i18n from 'i18n';

import './index.scss';

const accountFields = [ 'firstName', 'lastName', 'address1', 'address2', 'city', 'state', 'postalCode' ];

const mapStateToProps = state => ({
  accountDetails: state.accountDetails.toJSON(),
  activation: !state.activate.isEmpty(),
  language: state.language.get('current')
});

const { accountDetailsActions, logoutActions } = actions;
const mapDispatchToProps = { ...accountDetailsActions, ...logoutActions };

const styles = theme => ({
  button: {
    marginRight: theme.spacing.unit
  }
});

@withStyles(styles)
@connect(mapStateToProps, mapDispatchToProps)
export default class AccountDetailsView extends Component {
  static propTypes = {
    accountDetails: PropTypes.shape({
      address1: PropTypes.string,
      address2: PropTypes.string,
      city: PropTypes.string,
      country: PropTypes.string,
      firstName: PropTypes.string,
      postalCode: PropTypes.string,
      state: PropTypes.string,
      userId: PropTypes.string
    }),
    activation: PropTypes.bool.isRequired,
    classes: PropTypes.shape({
      button: PropTypes.string.isRequired
    }).isRequired,
    email: PropTypes.string.isRequired,
    getAccountDetails: PropTypes.func.isRequired,
    language: PropTypes.string.isRequired,
    setAccountDetails: PropTypes.func.isRequired,
    submitLogout: PropTypes.func.isRequired
  };

  static defaultProps = {
    accountDetails: {}
  }

  state = {
    activeStep: 1,
    ...AccountDetailsView.getStateAccountDetails(this.props.accountDetails)
  }

  handleChange = this::handleChange;

  componentWillMount() {
    const userId = _.get(this.props.accountDetails, 'userId');
    !userId && this.props.getAccountDetails(this.props.email);
  }

  componentWillReceiveProps(nextProps) {
    const userId = _.get(this.props.accountDetails, 'userId');
    !userId && this.setState(AccountDetailsView.getStateAccountDetails(nextProps.accountDetails));
  }

  static getStateAccountDetails(accountDetails) {
    return _.transform(accountFields, (memo, field) => { memo[field] = _.get(accountDetails, field, ''); }, {});
  }

  getStepContent() {
    switch (this.state.activeStep) {
      case 0:
        return (<AccountDetailsStep0 email={this.props.email} />);
      case 1: {
        const { activation, language } = this.props;
        const props = { activation, language, ..._.pick(this.state, accountFields) };
        return (<AccountDetailsStep1 {...props} handleChange={this.handleChange} />);
      }
      case 2: {
        return (<AccountDetailsStep2 language={this.props.language} />);
      }
      case 3:
        return (<AccountDetailsStep3 language={this.props.language} />);
    } /* istanbul ignore next */
    throw new Error(`Invalid step: ${this.state.activeStep}`);
  }

  handleBack = () => {
    this.setState(prevState => ({ activeStep: prevState.activeStep - 1 }));
  };

  handleLogout = () => {
    this.props.submitLogout();
  }

  handleNext = () => {
    const { activeStep } = this.state;
    if (_.includes([ 1, 2 ], activeStep)) {
      const userId = _.get(this.props.accountDetails, 'userId');
      const {
        firstName, lastName, address1, address2, city, state, postalCode
      } = this.state;
      this.props.setAccountDetails(userId, {
        firstName,
        lastName,
        address1,
        address2,
        city,
        state,
        postalCode
      });
    }
    this.setState(prevState => ({
      activeStep: prevState.activeStep + 1
    }));
  };

  render() {
    const { classes, language } = this.props;
    const { activeStep } = this.state;
    const {
      accountDetails: { steps },
      navButtons: { completeRegistrationButtonText, backButtonText, logoutButtonText, nextButtonText }
    } = i18n[language];
    return (
      <Grid container className='account-details card-container' justify='center'>
        <Grid item lg={4} md={6} sm={8} xs={12}>
          <Card>
            <CardContent>
              <Stepper activeStep={activeStep} className='stepper'>
                {steps.map(label => (
                  <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                  </Step>))}
              </Stepper>
              <div>
                {(
                  <div>
                    <div className='step-content'>
                      {this.getStepContent()}
                    </div>
                    <div>
                      <Grid container justify='space-between'>
                        <Grid item>
                          <Button
                            className={classes.button}
                            disabled={activeStep === 0}
                            onClick={this.handleBack}
                          >
                            {backButtonText}
                          </Button>
                          {activeStep !== 3 ? (
                            <Button
                              color='primary'
                              variant='contained'
                              onClick={this.handleNext}
                            >
                              {activeStep === steps.length - 1 ? completeRegistrationButtonText : nextButtonText}
                            </Button>) : null}
                        </Grid>
                        <Grid item>
                          <Button color='secondary' onClick={this.handleLogout}>{logoutButtonText}</Button>
                        </Grid>
                      </Grid>
                    </div>
                  </div>
                )}
              </div>
            </CardContent>
          </Card>
          {_.get(this.props.accountDetails, 'userId') ? null : <Overlay />}
        </Grid>
      </Grid>
    );
  }
}
