import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

export default class AccountDetailsStep0 extends PureComponent {
  static propTypes = {
    email: PropTypes.string.isRequired
  };

  render() {
    return (
      <Grid container alignItems='center' justify='center'>
        <Grid item>{this.props.email}</Grid>
      </Grid>
    );
  }
}
